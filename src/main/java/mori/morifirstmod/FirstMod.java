package mori.morifirstmod;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import mori.morifirstmod.items.ItemCustomAxe;
import mori.morifirstmod.items.ItemCustomAxeShovel;
import mori.morifirstmod.items.ItemCustomPickaxeAxe;
import mori.morifirstmod.items.ItemCustomPickaxeAxeShovel;
import mori.morifirstmod.items.ItemCustomPickaxeShovel;
import mori.morifirstmod.lists.BlockList;
import mori.morifirstmod.lists.ItemList;
import mori.morifirstmod.lists.ToolMaterialList;
import mori.morifirstmod.world.OreGeneration;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.HoeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemTier;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ShovelItem;
import net.minecraft.item.SwordItem;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod("morifirstmod")
public class FirstMod 
{
	public static FirstMod instance;
	public static final String modid = "morifirstmod";
	private static final Logger logger = LogManager.getLogger(modid);
	
	public static final ItemGroup first = new FirstItemGroup();
	
	public FirstMod() 
	{
		instance = this;
		
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistries);
		
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	private void setup(final FMLCommonSetupEvent event)
	{
		OreGeneration.setupOreGeneration();
		logger.info("Setup method registered.");
	}
	
	private void clientRegistries(final FMLClientSetupEvent event)
	{
		logger.info("clientRegistries method registered.");
	}
	
	@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
	public static class RegistryEvents
	{
		
		@SubscribeEvent
		public static void registerItems(final RegistryEvent.Register<Item> event)
		{
			event.getRegistry().registerAll
			(
				ItemList.bronze_ingot = new Item(new Item.Properties().group(first)).setRegistryName(location("bronze_ingot")),
				ItemList.tin_ingot = new Item(new Item.Properties().group(first)).setRegistryName(location("tin_ingot")),
				ItemList.copper_ingot = new Item(new Item.Properties().group(first)).setRegistryName(location("copper_ingot")),
				ItemList.ruby = new Item(new Item.Properties().group(first)).setRegistryName(location("ruby")),
				ItemList.amethyst = new Item(new Item.Properties().group(first)).setRegistryName(location("amethyst")),
				ItemList.saphire = new Item(new Item.Properties().group(first)).setRegistryName(location("saphire")),
				ItemList.topaz = new Item(new Item.Properties().group(first)).setRegistryName(location("topaz")),
				ItemList.luminstone = new Item(new Item.Properties().group(first)).setRegistryName(location("luminstone")),
				ItemList.rocksalt = new Item(new Item.Properties().group(first)).setRegistryName(location("rocksalt")),
								
				
				
							
				//Bronze Tools
				ItemList.bronze_axe = new ItemCustomAxe(ToolMaterialList.bronze, 7.0f, -3.0f, new Item.Properties().group(first)).setRegistryName(location("bronze_axe")),
				ItemList.bronze_pickaxeaxe = new ItemCustomPickaxeAxe(ToolMaterialList.bronze, 7, -2.8f, new Item.Properties().group(first)).setRegistryName(location("bronze_pickaxeaxe")),
				ItemList.bronze_pickaxeaxeshovel = new ItemCustomPickaxeAxeShovel(ToolMaterialList.bronze, 7, -2.8f, new Item.Properties().group(first)).setRegistryName(location("bronze_pickaxeaxeshovel")),
				ItemList.bronze_pickaxeshovel = new ItemCustomPickaxeShovel(ToolMaterialList.bronze, 7, -2.8f, new Item.Properties().group(first)).setRegistryName(location("bronze_pickaxeshovel")),
				ItemList.bronze_axeshovel = new ItemCustomAxeShovel(ToolMaterialList.bronze, 7, -2.8f, new Item.Properties().group(first)).setRegistryName(location("bronze_axeshovel")),
									
				
				ItemList.bronze_pickaxe = new PickaxeItem(ToolMaterialList.bronze, 2, -2.8f, new Item.Properties().group(first)).setRegistryName(location("bronze_pickaxe")),
				ItemList.bronze_hoe = new HoeItem(ToolMaterialList.bronze, -1.5f, new Item.Properties().group(first)).setRegistryName(location("bronze_hoe")),
				ItemList.bronze_shovel = new ShovelItem(ToolMaterialList.bronze, 2.0f, -3.0f, new Item.Properties().group(first)).setRegistryName(location("bronze_shovel")),
				ItemList.bronze_sword = new SwordItem(ToolMaterialList.bronze, 4, -2.4f, new Item.Properties().group(first)).setRegistryName(location("bronze_sword")),
				
				ItemList.dia_sword = new SwordItem(ItemTier.DIAMOND, 4, -2.4f, new Item.Properties().group(first)).setRegistryName(location("dia_sword")),
						
				
				//Additional Iron Tools
				ItemList.iron_pickaxeaxe = new ItemCustomPickaxeAxe(ItemTier.IRON, 6, -2.8f, new Item.Properties().group(first)).setRegistryName(location("iron_pickaxeaxe")),
				ItemList.iron_pickaxeshovel = new ItemCustomPickaxeShovel(ItemTier.IRON, 2, -2.4f, new Item.Properties().group(first)).setRegistryName(location("iron_pickaxeshovel")),
				ItemList.iron_pickaxeaxeshovel = new ItemCustomPickaxeAxeShovel(ItemTier.IRON, 2, -2.4f, new Item.Properties().group(first)).setRegistryName(location("iron_pickaxeaxeshovel")),
				ItemList.iron_axeshovel = new ItemCustomAxeShovel(ItemTier.IRON, 2, -2.4f, new Item.Properties().group(first)).setRegistryName(location("iron_axeshovel")),	
				
				//Ores
				ItemList.bronze_block = new BlockItem(BlockList.bronze_block, new Item.Properties().group(first)).setRegistryName(BlockList.bronze_block.getRegistryName()),
				ItemList.copper_ore = new BlockItem(BlockList.copper_ore, new Item.Properties().group(first)).setRegistryName(BlockList.copper_ore.getRegistryName()),
				ItemList.tin_ore = new BlockItem(BlockList.tin_ore, new Item.Properties().group(first)).setRegistryName(BlockList.tin_ore.getRegistryName()),
				ItemList.saphire_ore = new BlockItem(BlockList.saphire_ore, new Item.Properties().group(first)).setRegistryName(BlockList.saphire_ore.getRegistryName()),
				ItemList.topaz_ore = new BlockItem(BlockList.topaz_ore, new Item.Properties().group(first)).setRegistryName(BlockList.topaz_ore.getRegistryName()),
				ItemList.amethyst_ore = new BlockItem(BlockList.amethyst_ore, new Item.Properties().group(first)).setRegistryName(BlockList.amethyst_ore.getRegistryName()),
				ItemList.silver_ore = new BlockItem(BlockList.silver_ore, new Item.Properties().group(first)).setRegistryName(BlockList.silver_ore.getRegistryName()),
				ItemList.aluminium_ore = new BlockItem(BlockList.aluminium_ore, new Item.Properties().group(first)).setRegistryName(BlockList.aluminium_ore.getRegistryName()),
				ItemList.lead_ore = new BlockItem(BlockList.lead_ore, new Item.Properties().group(first)).setRegistryName(BlockList.lead_ore.getRegistryName()),
				ItemList.darkmetal_ore = new BlockItem(BlockList.darkmetal_ore, new Item.Properties().group(first)).setRegistryName(BlockList.darkmetal_ore.getRegistryName()),
				ItemList.lightmetal_ore = new BlockItem(BlockList.lightmetal_ore, new Item.Properties().group(first)).setRegistryName(BlockList.lightmetal_ore.getRegistryName()),
				ItemList.rocksalt_ore = new BlockItem(BlockList.rocksalt_ore, new Item.Properties().group(first)).setRegistryName(BlockList.rocksalt_ore.getRegistryName()),		
				ItemList.lumin_ore = new BlockItem(BlockList.lumin_ore, new Item.Properties().group(first)).setRegistryName(BlockList.lumin_ore.getRegistryName()),	
								
				ItemList.ruby_ore = new BlockItem(BlockList.ruby_ore, new Item.Properties().group(first)).setRegistryName(BlockList.ruby_ore.getRegistryName())
			
				
				);
			
			logger.info("Items registered");
		}
		
		@SubscribeEvent
		public static void registerBlocks(final RegistryEvent.Register<Block> event)
		{
			event.getRegistry().registerAll
			(
				BlockList.bronze_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(0).sound(SoundType.METAL)).setRegistryName(location("bronze_block")),
				BlockList.copper_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("copper_ore")),
				BlockList.tin_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("tin_ore")),
				BlockList.ruby_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("ruby_ore")),
				BlockList.saphire_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("saphire_ore")),
				BlockList.topaz_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("topaz_ore")),	
				BlockList.amethyst_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("amethyst_ore")),
				BlockList.aluminium_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("aluminium_ore")),
				BlockList.lead_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("lead_ore")),
				BlockList.darkmetal_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(15.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("darkmetal_ore")),
				BlockList.lightmetal_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(15.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("lightmetal_ore")),
				BlockList.rocksalt_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("rocksalt_ore")),
				BlockList.lumin_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("lumin_ore")),
						
				
				BlockList.silver_ore = new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 15.0f).lightValue(0).sound(SoundType.STONE)).setRegistryName(location("silver_ore"))
				
				
				
				);
			
			logger.info("Blocks registered");
		}
		
		
		
		
		
		private static ResourceLocation location(String name)
		{
			return new ResourceLocation(modid, name);
		}
		
		
		
		
		
		
		
		
		
		
		
	}
	
}
